import { CurrentAction, IMoviesData } from './utils/interfaces';
import {
    getPopularMovies,
    getTopRatedMovies,
    getUpcomingMovies,
    searchMovies,
} from './utils/api';
import {
    renderFavoriteFilms,
    renderMovies,
    resetFilms,
} from './utils/renderMethods';

export async function render(): Promise<void> {
    let moviesData: IMoviesData;
    let currentAction: CurrentAction = CurrentAction.Popular;

    // GET POPULAR TITLES ON PAGE START
    moviesData = await getPopularMovies();
    renderMovies(moviesData.movies);

    // RENDER FAVORITE FILMS
    await renderFavoriteFilms();

    // LOAD MORE FILMS ON LOAD MORE BTN CLICK
    const loadMoreBtn = document.getElementById(
        'load-more'
    ) as HTMLButtonElement;

    loadMoreBtn.addEventListener('click', async () => {
        if (currentAction === CurrentAction.Search) {
            moviesData = await searchMovies(
                searchInput.value,
                moviesData.page + 1
            );
        } else if (currentAction === CurrentAction.Popular) {
            moviesData = await getPopularMovies(moviesData.page + 1);
        } else if (currentAction === CurrentAction.Upcoming) {
            moviesData = await getUpcomingMovies(moviesData.page + 1);
        } else if (currentAction === CurrentAction.TopRated) {
            moviesData = await getTopRatedMovies(moviesData.page + 1);
        }
        renderMovies(moviesData.movies);
    });

    // GET POPULAR FILMS ON POPULAR BTN CLICK
    const popularRadioBtn = document.getElementById(
        'popular'
    ) as HTMLInputElement;
    popularRadioBtn.addEventListener('change', async () => {
        if (popularRadioBtn.value) {
            resetFilms();
            currentAction = CurrentAction.Popular;
            moviesData = await getPopularMovies();
            renderMovies(moviesData.movies);
        }
    });

    // GET UPCOMING FILMS ON UPCOMING BTN CLICK
    const upcomingRadioBtn = document.getElementById(
        'upcoming'
    ) as HTMLInputElement;
    upcomingRadioBtn.addEventListener('change', async () => {
        if (upcomingRadioBtn.value) {
            resetFilms();
            currentAction = CurrentAction.Upcoming;
            moviesData = await getUpcomingMovies();
            renderMovies(moviesData.movies);
        }
    });

    // GET TOP RATED FILMS ON TOP RATED BTN CLICK
    const topRatedRadioBtn = document.getElementById(
        'top_rated'
    ) as HTMLInputElement;
    topRatedRadioBtn.addEventListener('change', async () => {
        if (topRatedRadioBtn.value) {
            resetFilms();
            currentAction = CurrentAction.TopRated;
            moviesData = await getTopRatedMovies();
            renderMovies(moviesData.movies);
        }
    });

    // SEARCH BY NAME
    const submitBtn = document.getElementById('submit') as HTMLButtonElement;
    const searchInput = document.getElementById('search') as HTMLInputElement;

    submitBtn.addEventListener('click', async () => {
        resetFilms();
        currentAction = CurrentAction.Search;
        moviesData = await searchMovies(searchInput.value);
        renderMovies(moviesData.movies);
    });
}
