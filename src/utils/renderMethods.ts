import { IMovie } from './interfaces';
import { getMovie } from './api';
import { movieCardTemplate, randomFilmTemplate } from './templates';

export const resetFilms = (): void => {
    const filmContainerDiv = document.getElementById(
        'film-container'
    ) as HTMLDivElement;
    filmContainerDiv.innerHTML = '';
};

export const renderMovies = (movies: IMovie[]): void => {
    const filmContainerDiv = document.getElementById(
        'film-container'
    ) as HTMLDivElement;

    movies.map((movie) => {
        filmContainerDiv.innerHTML += `<div class="col-lg-3 col-md-4 col-12 p-2">
        ${movieCardTemplate(movie, 'movie', 'heartBtn')}</div>`;
    });

    bindEventListenerToClass('.heartBtn');
    renderRandomFilmHeader(movies);
};
export const renderRandomFilmHeader = (movies: IMovie[]): void => {
    const randomIndex = Math.floor(Math.random() * movies.length);
    const randomFilmContainerDiv = document.getElementById(
        'random-movie'
    ) as HTMLDivElement;

    randomFilmContainerDiv.innerHTML = randomFilmTemplate(movies[randomIndex]);
};

export async function addFilmToFavoritePanel(movieId: number): Promise<void> {
    const favoriteFilmsContainer = document.getElementById(
        'favorite-movies'
    ) as HTMLDivElement;
    const movieInfo = await getMovie(movieId);
    favoriteFilmsContainer.innerHTML += movieCardTemplate(
        movieInfo,
        'favorite',
        'favoriteHeartBtn'
    );
    bindEventListenerToClass('.favoriteHeartBtn');
}

export const renderFavoriteFilms = async (): Promise<void> => {
    const storedFavoriteFilms = window.localStorage.getItem('favoriteFilms');
    const favoriteFilms = storedFavoriteFilms
        ? JSON.parse(storedFavoriteFilms)
        : [];
    const favoriteFilmsContainer = document.getElementById(
        'favorite-movies'
    ) as HTMLDivElement;
    favoriteFilmsContainer.innerHTML = '';
    for (const movieId of favoriteFilms) {
        await addFilmToFavoritePanel(movieId);
    }
};

export const bindEventListenerToClass = (className: string): void => {
    const elements = document.querySelectorAll(className);
    for (let i = 0; i < elements.length; i++) {
        const self = elements[i];

        self.addEventListener('click', (event) => {
            event.preventDefault();
            editFavoriteList(parseInt(self.id.split('|')[1]));
        });
    }
};


export const editFavoriteList = async (id: number): Promise<void> => {
    const storedFavoriteFilms = window.localStorage.getItem('favoriteFilms');
    const favoriteFilms = storedFavoriteFilms
        ? JSON.parse(storedFavoriteFilms)
        : [];

    const isFavorite = favoriteFilms.indexOf(id) !== -1;
    const updatedFavoriteFilms = !isFavorite
        ? [...favoriteFilms, id]
        : favoriteFilms.filter((filmId: number) => filmId !== id);
    localStorage.setItem('favoriteFilms', JSON.stringify(updatedFavoriteFilms));

    if (isFavorite) {
        // update heart icon opacity
        document
            .getElementById(`movie|${id}`)
            ?.setAttribute('fill-opacity', '0.5');
        // remove film  from Favorite Panel
        const unFavoredFilmDiv = document.getElementById(`favorite|${id}`)
            ?.parentNode as HTMLDivElement;
        unFavoredFilmDiv?.parentNode?.removeChild(unFavoredFilmDiv);
    } else {
        // update heart icon opacity
        document
            .getElementById(`movie|${id}`)
            ?.setAttribute('fill-opacity', '1');
        // add film to Favorite Panel
        await addFilmToFavoritePanel(id);
    }
};

export const getIsFavorite = (id: number): boolean => {
    const storedFavoriteFilms = window.localStorage.getItem('favoriteFilms');
    const favoriteFilms = storedFavoriteFilms
        ? JSON.parse(storedFavoriteFilms)
        : [];
    return favoriteFilms.indexOf(id) == -1 ? false : true;
};
