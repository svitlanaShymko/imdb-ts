import { IMovie } from './interfaces';
import { getIsFavorite } from './renderMethods';

export const movieCardTemplate = (
    { id, posterPath, overview, releaseDate }: IMovie,
    idPrefix: string,
    className: string
): string =>
    `<div class="card shadow-sm">
    <img
        src="https://image.tmdb.org/t/p/original${posterPath}"
    />
    <svg
    style="cursor:pointer;"
    id="${idPrefix}|${id}"
        xmlns="http://www.w3.org/2000/svg"
        stroke="red"
        fill="red"
        fill-opacity="${getIsFavorite(id) ? 1 : 0.5}"
        width="50"
        height="50"
        class="${className} bi bi-heart-fill position-absolute p-2"
        viewBox="0 -2 18 22"
    >
        <path
            fill-rule="evenodd"
            d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
        />
    </svg>
    <div class="card-body">
        <p class="card-text truncate">
        ${overview}
        </p>
        <div
            class="
                d-flex
                justify-content-between
                align-items-center
            "
        >
            <small class="text-muted">${releaseDate}</small>
        </div>
    </div>
</div>`;

export const randomFilmTemplate = ({
    originalTitle,
    overview,
    posterPath,
}: IMovie): string =>
    `<div class="row py-lg-5" style="background-image: url(https://image.tmdb.org/t/p/original${posterPath}); background-size: 100% auto;">
  <div
      class="col-lg-6 col-md-8 mx-auto"
      style="background-color: #2525254f";
  >
      <h1 id="random-movie-name" class="fw-light text-light">${originalTitle}</h1>
      <p id="random-movie-description" class="lead text-white">
      ${overview}
      </p>
  </div>
</div>`;
