import { IMovie } from './interfaces';

export const mapObjectToIMovie = (movieData: {
  id: number;
  original_title: string;
  overview: string;
  poster_path: string;
  release_date: string;
}): IMovie => {
  const { id, original_title, overview, poster_path, release_date } = movieData;
  return {
    id,
    originalTitle: original_title,
    overview,
    posterPath: poster_path,
    releaseDate: release_date,
  };
};

export const mapArrayToIMovieArray = (
  movies: {
    id: number;
    original_title: string;
    overview: string;
    poster_path: string;
    release_date: string;
  }[],
): IMovie[] => {
  return movies.map((movie) => mapObjectToIMovie(movie));
};
