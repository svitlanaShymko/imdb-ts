import { IMovie, IMoviesData } from './interfaces';
import { axiosMovies as axios } from './axios';
import { mapArrayToIMovieArray, mapObjectToIMovie } from './mappers';

export const getPopularMovies = async (page = 1): Promise<IMoviesData> => {
  const { data } = await axios.get(`/movie/popular`, {
    params: { page },
  });
  return {page: data.page, movies: mapArrayToIMovieArray(data.results)};
};

export const getTopRatedMovies = async (page = 1): Promise<IMoviesData> => {
  const { data } = await axios.get(`/movie/top_rated`, {
    params: { page },
  });

  return {page: data.page, movies: mapArrayToIMovieArray(data.results)};
};

export const getUpcomingMovies = async (page = 1): Promise<IMoviesData> => {
  const { data } = await axios.get(`/movie/upcoming`, {
    params: { page },
  });
  return {page: data.page, movies: mapArrayToIMovieArray(data.results)};
};

export const searchMovies = async (query: string, page = 1): Promise<IMoviesData> => {
  const { data } = await axios.get(`/search/movie`, {
    params: { query, page },
  });
  return {page: data.page, movies: mapArrayToIMovieArray(data.results)};
};

export const getMovie = async (id: number): Promise<IMovie> => {
  const { data } = await axios.get(`/movie/${id}`);
  return mapObjectToIMovie(data);
};
