export interface IMoviesData {
    page: number;
    movies: IMovie[];
}

export interface IMovie {
    id: number;
    originalTitle: string;
    overview: string;
    posterPath: string;
    releaseDate: string;
}

export enum CurrentAction {
    Search = 'Search',
    Popular = 'Popular',
    Upcoming = 'Upcoming',
    TopRated = 'TopRated'
} 
