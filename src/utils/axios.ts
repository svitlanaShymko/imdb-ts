
import axios from 'axios';

export const axiosMovies = axios.create({
  baseURL: 'https://api.themoviedb.org/3',
  params: {
    api_key: '94c978c5b4851159a854ef31b5afcd21',
    language: 'en-US'
  }
});