# IMDB Typescript Project #

Basic Typescript/HTML/CSS project using [IMDB API](https://developers.themoviedb.org/3/movies/get-movie-details), and Bootstrap library for styles


### Possibilities ###

* Search films
* Show popular films
* Show upcoming films
* Show top rated films
* Pagination
* Random film info in the top of the page
* Add/edit Favorite films


### To run application ###
```
npm run dev
```

### To compile Typescript code ###
```
npx tsc
```


### Application Screens ###
* App:
![Screenshot](public/screen0.png)

* Favorite Panel:
![Screenshot](public/screen1.png)